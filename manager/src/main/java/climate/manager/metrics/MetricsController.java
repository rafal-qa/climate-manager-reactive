package climate.manager.metrics;

import climate.manager.data.model.DataAmqp;
import climate.manager.utils.RabbitDataConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.rabbitmq.Receiver;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/metrics")
public class MetricsController {

    private static final Duration REFRESH_RATE = Duration.ofSeconds(5);

    private final MetricsService metricsService;
    private final Receiver receiver;
    private final RabbitDataConverter dataConverter;

    @Value("${amqp.queue-metrics-name}")
    private String queueMetricsName;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> getMetrics() {

        return receiver.consumeAutoAck(queueMetricsName)
                .map(delivery -> dataConverter.fromBytes(delivery.getBody()))
                .buffer(REFRESH_RATE)
                .map(this::discardOutdated)
                .map(metricsService::calculateAvg)
                .map(metricsService::generateTextReport);
    }

    private List<DataAmqp> discardOutdated(List<DataAmqp> newItems) {
        LocalDateTime expiryTime = LocalDateTime.now().minus(REFRESH_RATE);

        newItems.removeIf(data -> data.getTimestamp().isBefore(expiryTime));

        return newItems;
    }
}
