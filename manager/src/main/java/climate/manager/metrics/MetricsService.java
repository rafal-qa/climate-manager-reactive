package climate.manager.metrics;

import climate.manager.data.model.DataAmqp;
import climate.manager.data.model.DataType;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.lang.String.format;
import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.groupingBy;

@Service
public class MetricsService {

    public Map<String, Map<DataType, Double>> calculateAvg(List<DataAmqp> dataList) {
        Map<String, Map<DataType, Double>> resultsForAllDevices = new TreeMap<>();

        Map<String, List<DataAmqp>> dataAmqpByDevice =
                dataList.stream().collect(groupingBy(DataAmqp::getDeviceId));

        for (Map.Entry<String, List<DataAmqp>> deviceEntry : dataAmqpByDevice.entrySet()) {
            Map<DataType, Double> resultsForDevice = new HashMap<>();

            deviceEntry.getValue().stream()
                    .collect(groupingBy(DataAmqp::getDataType))
                    .forEach(
                            (dataType, dataListForDataType) ->
                                    resultsForDevice.put(
                                            dataType, calculateAvgForSingleDataType(dataListForDataType)));

            resultsForAllDevices.put(deviceEntry.getKey(), resultsForDevice);
        }

        return resultsForAllDevices;
    }

    private Double calculateAvgForSingleDataType(List<DataAmqp> dataList) {
        return dataList.stream().collect(averagingDouble(DataAmqp::getValue));
    }

    public String generateTextReport(Map<String, Map<DataType, Double>> avgCalculationsResult) {
        String headerFormat = "| %-12s | %-11s | %-11s |";
        String dataFormat = "| %-12s | %11s | %11s |";
        String separator = "+--------------+-------------+-------------+";

        String header = format(headerFormat, "Device ID", DataType.TEMPERATURE, DataType.HUMIDITY);

        List<String> outputLines = new ArrayList<>(List.of(separator, header, separator));

        for (Map.Entry<String, Map<DataType, Double>> entry : avgCalculationsResult.entrySet()) {
            Map<DataType, Double> deviceData = entry.getValue();

            outputLines.add(
                    format(
                            dataFormat,
                            entry.getKey(),
                            getValueAsText(deviceData, DataType.TEMPERATURE),
                            getValueAsText(deviceData, DataType.HUMIDITY)));
        }

        outputLines.add(separator);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        outputLines.add(" " + LocalDateTime.now().format(formatter));

        return String.join(format("%n"), outputLines);
    }

    private String getValueAsText(Map<DataType, Double> deviceData, DataType dataType) {
        if (deviceData.containsKey(dataType)) {
            return format("%.1f", deviceData.get(dataType));
        }

        return format("%-3s", "-");
    }
}
