package climate.manager.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.rabbitmq.Sender;

import javax.annotation.PostConstruct;
import java.util.Map;

import static reactor.rabbitmq.ResourcesSpecification.*;

@Slf4j
@RequiredArgsConstructor
@Component
public class RabbitSetup {

    private final Sender sender;

    @Value("${amqp.exchange-name}")
    private String exchangeName;

    @Value("${amqp.queue-mongodb-name}")
    private String queueMongodbName;

    @Value("${amqp.queue-metrics-name}")
    private String queueMetricsName;

    @PostConstruct
    public void init() {
        Map<String, Object> fiveSecondsTTL = Map.of("x-message-ttl", 5000);

        sender.declare(exchange(exchangeName).type("fanout").durable(true))
                .then(sender.declare(queue(queueMongodbName).durable(true)))
                .then(sender.declare(queue(queueMetricsName).arguments(fiveSecondsTTL).durable(true)))
                .then(sender.bind(binding(exchangeName, "", queueMongodbName)))
                .then(sender.bind(binding(exchangeName, "", queueMetricsName)))
                .subscribe(r -> log.info("RabbitMQ initialized"));
    }
}
