package climate.manager.config;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import reactor.rabbitmq.*;

@Configuration
public class RabbitConfiguration {

    @Bean
    Sender sender() {
        return RabbitFlux.createSender(
                new SenderOptions().connectionMono(createConnection("climate-manager.sender")));
    }

    @Bean
    Receiver receiver() {
        return RabbitFlux.createReceiver(
                new ReceiverOptions().connectionMono(createConnection("climate-manager.receiver")));
    }

    private Mono<Connection> createConnection(String name) {
        return Mono.fromCallable(() -> new ConnectionFactory().newConnection(name)).cache();
    }
}
