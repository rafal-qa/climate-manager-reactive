package climate.manager.status;

import climate.manager.device.Device;
import climate.manager.device.model.DeviceWithDataCount;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class StatusService {

    public String generateTextReport(List<DeviceWithDataCount> devicesWithDataCount) {
        String headerFormat = "| %-12s | %-10s | %-10s |";
        String dataFormat = "| %-12s | %-10s | %10s |";
        String separator = "+--------------+------------+------------+";

        String header = format(headerFormat, "Device ID", "Status", "Data count");

        List<String> outputLines = new ArrayList<>(List.of(separator, header, separator));

        for (DeviceWithDataCount deviceWithDataCount : devicesWithDataCount) {
            Device device = deviceWithDataCount.getDevice();
            Integer dataCount = deviceWithDataCount.getDataCount();

            outputLines.add(format(dataFormat, device.getDeviceId(), calculateDeviceStatus(device), dataCount));
        }

        outputLines.add(separator);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        outputLines.add(" " + LocalDateTime.now().format(formatter));

        return String.join(format("%n"), outputLines);
    }

    private String calculateDeviceStatus(Device device) {
        LocalDateTime minAliveTime = LocalDateTime.now().minusSeconds(15);
        LocalDateTime lastSeen = device.getLastSeenCommand();

        if (lastSeen == null || lastSeen.isBefore(minAliveTime)) {
            return "DOWN";
        }

        return "RUNNING";
    }
}
