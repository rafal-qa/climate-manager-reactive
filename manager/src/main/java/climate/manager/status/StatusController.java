package climate.manager.status;

import climate.manager.device.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;

@RequiredArgsConstructor
@RestController
@RequestMapping("/status")
public class StatusController {

    private static final Duration REFRESH_RATE = Duration.ofSeconds(5);

    private final DeviceService deviceService;
    private final StatusService statusService;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> getStatus() {
        return Flux.interval(Duration.ZERO, REFRESH_RATE)
                .flatMap(i -> deviceService.getAllDevicesWithDataCount())
                .map(statusService::generateTextReport);
    }
}
