package climate.manager.data;

import climate.manager.data.model.DataAmqp;
import climate.manager.data.model.DataRequest;
import climate.manager.utils.RabbitDataConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.rabbitmq.OutboundMessage;
import reactor.rabbitmq.Sender;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/data")
public class DataController {

    private final Sender sender;
    private final RabbitDataConverter dataConverter;

    @Value("${amqp.exchange-name}")
    private String exchangeName;

    @PostMapping(path = "{deviceId}", consumes = MediaType.APPLICATION_NDJSON_VALUE)
    public Mono<Void> consumeDeviceData(@PathVariable String deviceId, @RequestBody Flux<DataRequest> dataRequestFlux) {

        Flux<OutboundMessage> outboundFlux = dataRequestFlux
                .doOnSubscribe(subscription -> log.info("OnSubscribe"))
                .doOnNext(data -> log.info("[{}] Consuming data {}", deviceId, data.getTimestamp()))
                .doOnCancel(() -> log.info("OnCancel"))
                .doOnTerminate(() -> log.info("OnTerminate"))
                .doOnError(e -> log.info("OnError: " + e.getMessage()))
                .doFinally(signalType -> log.info("Finally: " + signalType.toString()))
                .map(dataRequest ->
                        DataAmqp.builder()
                                .deviceId(deviceId)
                                .dataType(dataRequest.getDataType())
                                .value(dataRequest.getValue())
                                .timestamp(dataRequest.getTimestamp())
                                .build())
                .map(dataAmqp -> new OutboundMessage(exchangeName, "", dataConverter.toBytes(dataAmqp)));

        return sender.send(outboundFlux);
    }
}
