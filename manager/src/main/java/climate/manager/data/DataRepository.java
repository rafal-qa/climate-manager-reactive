package climate.manager.data;

import climate.manager.data.model.DataCountByDevice;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface DataRepository extends ReactiveMongoRepository<Data, String> {

    @Aggregation("{ $group: { _id: '$deviceId', count: { $count: {} } } }")
    Flux<DataCountByDevice> countGroupingByDeviceId();
}
