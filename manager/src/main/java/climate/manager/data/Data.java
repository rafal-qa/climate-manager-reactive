package climate.manager.data;

import climate.manager.data.model.DataType;
import lombok.Builder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@lombok.Data
@Builder
@Document
public class Data {
    @Id
    public String id;
    @Indexed
    private String deviceId;
    private DataType dataType;
    private Double value;
    private LocalDateTime timestamp;
}
