package climate.manager.data.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class DataAmqp {
    private String deviceId;
    private DataType dataType;
    private Double value;
    private LocalDateTime timestamp;
}
