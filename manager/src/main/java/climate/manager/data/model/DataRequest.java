package climate.manager.data.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DataRequest {
    private DataType dataType;
    private Double value;
    private LocalDateTime timestamp;
}
