package climate.manager.data.model;

public enum DataType {
    TEMPERATURE,
    HUMIDITY
}
