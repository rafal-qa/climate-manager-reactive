package climate.manager.data.model;

import lombok.Data;

@Data
public class DataCountByDevice {
    private String _id;
    private Integer count;
}
