package climate.manager.data;

import climate.manager.data.model.DataAmqp;
import climate.manager.utils.RabbitDataConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.rabbitmq.RabbitFluxException;
import reactor.rabbitmq.Receiver;
import reactor.util.retry.Retry;

import java.time.Duration;

@Slf4j
@RequiredArgsConstructor
@Component
public class RabbitToMongoRunner implements ApplicationRunner {

    private final DataRepository dataRepository;
    private final Receiver receiver;
    private final RabbitDataConverter dataConverter;

    @Value("${amqp.queue-mongodb-name}")
    private String queueMongodbName;

    @Override
    public void run(ApplicationArguments args) {
        Retry waitForRabbitSetup = Retry.fixedDelay(10, Duration.ofSeconds(1))
                .filter(throwable -> throwable instanceof RabbitFluxException);

        Flux<Data> dataFlux = receiver.consumeAutoAck(queueMongodbName)
                .retryWhen(waitForRabbitSetup)
                .doOnError(e -> log.info("OnError: " + e.getMessage()))
                .map(delivery -> dataConverter.fromBytes(delivery.getBody()))
                .map(this::mapRabbitToMongo);

        dataRepository.saveAll(dataFlux)
                .subscribe(data -> log.info("[{}] Saved in MongoDB {}", data.getDeviceId(), data.getTimestamp()));
    }

    private Data mapRabbitToMongo(DataAmqp dataAmqp) {
        return Data.builder()
                .deviceId(dataAmqp.getDeviceId())
                .dataType(dataAmqp.getDataType())
                .value(dataAmqp.getValue())
                .timestamp(dataAmqp.getTimestamp())
                .build();
    }
}
