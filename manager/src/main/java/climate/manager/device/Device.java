package climate.manager.device;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@Document
public class Device {
    @Id
    public String id;

    @Indexed(unique = true)
    private String deviceId;

    private String deviceName;
    private String commandResumeToken;
    private LocalDateTime lastSeenCommand;
}
