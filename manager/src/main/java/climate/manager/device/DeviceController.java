package climate.manager.device;

import climate.manager.device.model.DeviceRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
@RequestMapping("/device")
public class DeviceController {

    private final DeviceService deviceService;
    private final DeviceErrorService deviceErrorService;

    @PostMapping()
    public Mono<Device> registerDevice(@RequestBody DeviceRequest request) {
        return deviceService.register(request.getDeviceId(), request.getDeviceName())
                .onErrorResume(DuplicateKeyException.class, e -> deviceErrorService.deviceExists(request.getDeviceId()));
    }

    @GetMapping("{deviceId}")
    public Mono<Device> getDeviceDetails(@PathVariable String deviceId) {
        return deviceService.getByDeviceId(deviceId)
                .switchIfEmpty(Mono.defer(() -> deviceErrorService.deviceNotFound(deviceId)));
    }
}
