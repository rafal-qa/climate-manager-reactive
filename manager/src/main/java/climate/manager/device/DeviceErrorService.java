package climate.manager.device;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class DeviceErrorService {

    public Mono<Device> deviceExists(String deviceId) {
        return httpError(HttpStatus.BAD_REQUEST, String.format("Device '%s' is already registered", deviceId));
    }

    public Mono<Device> deviceNotFound(String deviceId) {
        return httpError(HttpStatus.NOT_FOUND, String.format("Device '%s' was not found", deviceId));
    }

    private Mono<Device> httpError(HttpStatus httpStatus, String message) {
        log.error(message);

        return Mono.error(new ResponseStatusException(httpStatus, message));
    }
}
