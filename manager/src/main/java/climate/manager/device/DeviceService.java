package climate.manager.device;

import climate.manager.data.DataRepository;
import climate.manager.data.model.DataCountByDevice;
import climate.manager.device.model.DeviceWithDataCount;
import lombok.RequiredArgsConstructor;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static climate.manager.utils.MongoBsonUtils.createDocument;
import static climate.manager.utils.MongoBsonUtils.extractString;

@Service
@RequiredArgsConstructor
public class DeviceService {

    private final DeviceRepository deviceRepository;
    private final DataRepository dataRepository;

    public Mono<Device> register(String deviceId, String deviceName) {
        return deviceRepository.save(
                Device.builder().deviceId(deviceId).deviceName(deviceName).commandResumeToken("").build());
    }

    public Mono<Device> getByDeviceId(String deviceId) {
        return deviceRepository.findByDeviceId(deviceId);
    }

    public Mono<Long> setLastSeenCommand(String deviceId, LocalDateTime timestamp) {
        return deviceRepository.findAndUpdateLastSeenCommandByDeviceId(deviceId, timestamp);
    }

    public Mono<BsonDocument> getCommandResumeToken(String deviceId) {
        return getByDeviceId(deviceId).map(device -> createDocument(device.getCommandResumeToken()));
    }

    public Mono<Long> setCommandResumeToken(String deviceId, BsonValue resumeToken) {
        return deviceRepository.findAndUpdateCommandResumeTokenByDeviceId(
                deviceId, extractString(resumeToken));
    }

    public Mono<List<DeviceWithDataCount>> getAllDevicesWithDataCount() {
        Mono<List<Device>> devicesMono = deviceRepository.findAll().collectList();
        Mono<List<DataCountByDevice>> countByDeviceMono =
                dataRepository.countGroupingByDeviceId().collectList();

        return Mono.zip(
                devicesMono,
                countByDeviceMono,
                (devices, countByDevice) -> {
                    List<DeviceWithDataCount> result = new ArrayList<>();

                    devices.forEach(device ->
                            result.add(DeviceWithDataCount.builder()
                                    .device(device)
                                    .dataCount(getCountByDeviceId(countByDevice, device.getDeviceId()))
                                    .build()));

                    return result;
                });
    }

    private Integer getCountByDeviceId(List<DataCountByDevice> allDataCount, String deviceId) {
        return allDataCount.stream()
                .filter(dataCount -> dataCount.get_id().equals(deviceId))
                .map(DataCountByDevice::getCount)
                .findFirst()
                .orElse(0);
    }
}
