package climate.manager.device.model;

import lombok.Data;

@Data
public class DeviceRequest {
    private String deviceId;
    private String deviceName;
}
