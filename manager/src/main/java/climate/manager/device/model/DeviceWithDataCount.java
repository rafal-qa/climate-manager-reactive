package climate.manager.device.model;

import climate.manager.device.Device;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeviceWithDataCount {
    private Device device;
    private Integer dataCount;
}
