package climate.manager.device;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Update;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Repository
public interface DeviceRepository extends ReactiveMongoRepository<Device, String> {

    Mono<Device> findByDeviceId(String deviceId);

    @Update("{ '$set' : { 'lastSeenCommand' : ?1 } }")
    Mono<Long> findAndUpdateLastSeenCommandByDeviceId(String deviceId, LocalDateTime lastSeenCommand);

    @Update("{ '$set' : { 'commandResumeToken' : ?1 } }")
    Mono<Long> findAndUpdateCommandResumeTokenByDeviceId(String deviceId, String token);
}
