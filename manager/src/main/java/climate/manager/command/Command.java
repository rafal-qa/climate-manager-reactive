package climate.manager.command;

import climate.manager.command.model.CommandType;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@Document
public class Command {
    @Id
    private String id;
    private String deviceId;
    private LocalDateTime timestamp;
    private CommandType commandType;
    private Double value;
}
