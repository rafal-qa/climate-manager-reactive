package climate.manager.command;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommandRepository extends ReactiveMongoRepository<Command, String> {
}
