package climate.manager.command;

import climate.manager.command.model.CommandRequest;
import climate.manager.command.model.CommandResponse;
import climate.manager.device.DeviceErrorService;
import climate.manager.device.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
@RequestMapping("/command")
public class CommandController {

    private final CommandService commandService;
    private final DeviceService deviceService;
    private final DeviceErrorService deviceErrorService;

    @PostMapping("{deviceId}")
    public Mono<Command> sendCommand(@PathVariable String deviceId, @RequestBody CommandRequest request) {
        return deviceService.getByDeviceId(deviceId)
                .switchIfEmpty(Mono.defer(() -> deviceErrorService.deviceNotFound(deviceId)))
                .flatMap(device -> commandService.save(deviceId, request.getCommandType(), request.getValue()));
    }

    @GetMapping(path = "{deviceId}", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<CommandResponse> produceCommandStream(@PathVariable String deviceId) {
        Flux<CommandResponse> changeStreamFlux = commandService.getChangeStream(deviceId);
        Flux<CommandResponse> pingFlux = commandService.getPingFlux(deviceId);
        Flux<CommandResponse> commandStreamFlux = Flux.merge(changeStreamFlux, pingFlux);

        return deviceService.getByDeviceId(deviceId)
                .switchIfEmpty(Mono.defer(() -> deviceErrorService.deviceNotFound(deviceId)))
                .flatMapMany(device -> commandStreamFlux);
    }
}
