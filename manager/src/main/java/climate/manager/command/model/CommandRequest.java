package climate.manager.command.model;

import lombok.Data;

@Data
public class CommandRequest {
    private CommandType commandType;
    private Double value;
}
