package climate.manager.command.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class CommandResponse {
    private LocalDateTime timestamp;
    private CommandType commandType;
    private Double value;
}
