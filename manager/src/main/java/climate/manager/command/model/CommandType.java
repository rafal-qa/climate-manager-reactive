package climate.manager.command.model;

public enum CommandType {
    TEMPERATURE,
    PING
}
