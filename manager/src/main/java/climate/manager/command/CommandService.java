package climate.manager.command;

import climate.manager.command.model.CommandResponse;
import climate.manager.command.model.CommandType;
import climate.manager.device.DeviceService;
import com.mongodb.MongoCommandException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.BsonDocument;
import org.springframework.data.mongodb.core.ChangeStreamEvent;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;

import static climate.manager.utils.MongoBsonUtils.createDocument;
import static climate.manager.utils.MongoBsonUtils.extractString;
import static java.util.Objects.requireNonNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommandService {

    private final ReactiveMongoTemplate mongoTemplate;
    private final DeviceService deviceService;
    private final CommandRepository commandRepository;

    public Mono<Command> save(String deviceId, CommandType commandType, Double value) {
        return commandRepository.save(Command.builder()
                .deviceId(deviceId)
                .timestamp(LocalDateTime.now())
                .commandType(commandType)
                .value(value)
                .build());
    }

    public Flux<CommandResponse> getChangeStream(String deviceId) {
        Mono<BsonDocument> resumeTokenMono = deviceService.getCommandResumeToken(deviceId);

        return resumeTokenMono.flatMapMany(token -> resumeChangeStream(deviceId, token));
    }

    private Flux<CommandResponse> resumeChangeStream(String deviceId, BsonDocument resumeToken) {
        return listenChangeStream(deviceId, resumeToken)
                .onErrorResume(MongoCommandException.class, e -> {
                    log.warn("Unable to resume change stream for '{}' device, skipping previous changes", deviceId);
                    var emptyToken = createDocument("");
                    return listenChangeStream(deviceId, emptyToken);
                })
                .flatMap(event -> deviceService.setCommandResumeToken(deviceId, requireNonNull(event.getResumeToken()))
                        .zipWith(Mono.just(event)))
                .map(tuple2 -> requireNonNull(tuple2.getT2().getBody()))
                .map(command -> CommandResponse.builder()
                        .timestamp(command.getTimestamp())
                        .commandType(command.getCommandType())
                        .value(command.getValue())
                        .build())
                .doOnNext(commandResponse -> log.info("Data was sent: " + commandResponse));
    }

    private Flux<ChangeStreamEvent<Command>> listenChangeStream(String deviceId, BsonDocument resumeToken) {
        var changeStream = mongoTemplate.changeStream(Command.class)
                .watchCollection("command")
                .filter(Criteria.where("deviceId").is(deviceId));

        if (extractString(resumeToken).isEmpty()) {
            return changeStream.listen();
        }

        return changeStream.startAfter(resumeToken).listen();
    }

    public Flux<CommandResponse> getPingFlux(String deviceId) {
        return Flux.interval(Duration.ZERO, Duration.ofSeconds(10))
                .map(i -> LocalDateTime.now())
                .flatMap(timestamp ->
                        deviceService.setLastSeenCommand(deviceId, timestamp).zipWith(Mono.just(timestamp)))
                .map(tuple2 -> pingResponse(tuple2.getT2()));
    }

    private static CommandResponse pingResponse(LocalDateTime timestamp) {
        return CommandResponse.builder().timestamp(timestamp).commandType(CommandType.PING).build();
    }
}
