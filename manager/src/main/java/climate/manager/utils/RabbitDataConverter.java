package climate.manager.utils;

import climate.manager.data.model.DataAmqp;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;

@RequiredArgsConstructor
@Component
public class RabbitDataConverter {

    private final ObjectMapper objectMapper;

    public byte[] toBytes(DataAmqp data) {
        try {
            return objectMapper.writeValueAsBytes(data);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public DataAmqp fromBytes(byte[] bytes) {
        try {
            return objectMapper.readValue(bytes, DataAmqp.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
