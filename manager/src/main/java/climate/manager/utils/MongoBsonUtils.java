package climate.manager.utils;

import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;

public class MongoBsonUtils {

    public static BsonDocument createDocument(String value) {
        return new BsonDocument("_data", new BsonString(value));
    }

    public static String extractString(BsonValue bson) {
        return bson.asDocument().get("_data").asString().getValue();
    }
}
