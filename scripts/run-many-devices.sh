#!/usr/bin/env bash

deviceNum=101
lastDeviceNum=200

until [ $deviceNum -gt $lastDeviceNum ]; do
  echo "Starting $deviceNum"

  deviceId="device-$deviceNum"
  deviceName="Device $deviceNum"

  curl -X POST http://localhost:8080/device \
    -H 'Content-Type: application/json' \
    -d "{\"deviceId\":\"$deviceId\",\"deviceName\":\"$deviceName\"}"

  DEVICE_ID=$deviceId java -jar ../device/target/device-0.0.1-SNAPSHOT.jar &

  rand=$((RANDOM % 1000))
  sleep 0.${rand}

  ((deviceNum++))
done
