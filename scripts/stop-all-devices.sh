#!/usr/bin/env bash

ps aux | grep 'device-0.0.1-SNAPSHOT.jar' | grep java | awk '{print $2}' | xargs -r kill
