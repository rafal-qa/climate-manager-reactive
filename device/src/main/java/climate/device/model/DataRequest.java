package climate.device.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class DataRequest {
    private DataType dataType;
    private Double value;
    private LocalDateTime timestamp;
}
