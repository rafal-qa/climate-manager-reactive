package climate.device.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CommandResponse {
    private LocalDateTime timestamp;
    private CommandType commandType;
    private Double value;
}
