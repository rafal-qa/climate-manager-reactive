package climate.device.model;

public enum DataType {
    TEMPERATURE,
    HUMIDITY
}
