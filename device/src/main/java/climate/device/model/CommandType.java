package climate.device.model;

public enum CommandType {
    TEMPERATURE,
    PING
}
