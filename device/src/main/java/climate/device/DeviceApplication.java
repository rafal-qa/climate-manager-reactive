package climate.device;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DeviceApplication {

    public static void main(String[] args) throws InterruptedException {
        ClimateClient climateClient = new ClimateClient();

        log.info("Device ID: " + climateClient.getDeviceId());

        climateClient.sendData();
        climateClient.receiveCommands();

        Thread.currentThread().join();
    }
}
