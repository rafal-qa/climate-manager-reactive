package climate.device;

import climate.device.model.CommandResponse;
import climate.device.model.DataRequest;
import climate.device.model.DataType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
public class ClimateClient {

    private static final Retry RETRY_FOR_60_SEC = Retry.fixedDelay(30, Duration.ofSeconds(2));

    private final WebClient client = WebClient.create("http://localhost:8080");
    private final String deviceId = System.getenv().getOrDefault("DEVICE_ID", "unnamed");

    public String getDeviceId() {
        return deviceId;
    }

    public void sendData() {
        Flux<DataRequest> dataFlux = Flux.interval(Duration.ofSeconds(1))
                .doOnNext(i -> log.info("Sending data " + i))
                .map(i -> generateData());

        client.post()
                .uri("/data/{deviceId}", deviceId)
                .contentType(MediaType.APPLICATION_NDJSON)
                .body(dataFlux, DataRequest.class)
                .retrieve()
                .bodyToMono(Void.class)
                .doOnSubscribe(subscription -> log.info("OnSubscribe"))
                .doOnError(e -> log.info("OnError: " + e.getMessage()))
                .retryWhen(RETRY_FOR_60_SEC)
                .subscribe();
    }

    public void receiveCommands() {
        client.get()
                .uri("/command/{deviceId}", deviceId)
                .retrieve()
                .bodyToFlux(CommandResponse.class)
                .doOnError(e -> log.info(e.getMessage()))
                .doOnCancel(() -> log.info("OnCancel"))
                .doOnTerminate(() -> log.info("OnTerminate"))
                .doFinally(signalType -> log.info("Finally: " + signalType.toString()))
                .retryWhen(RETRY_FOR_60_SEC)
                .subscribe(command -> log.info("Receiving command {} / {}",
                        command.getCommandType(), command.getTimestamp()));
    }

    private DataRequest generateData() {
        double randomValue = ThreadLocalRandom.current().nextInt(20, 60);
        int randomDataTypeIndex = new Random().nextInt(DataType.values().length);

        return DataRequest.builder()
                .dataType(DataType.values()[randomDataTypeIndex])
                .value(randomValue)
                .timestamp(LocalDateTime.now())
                .build();
    }
}
