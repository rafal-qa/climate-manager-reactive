# Reactive stack performance - Java demo project

*Climate Manager* is a **Spring Boot** application demonstrating usage of **Spring WebFlux** and **MongoDB**, **RabbitMQ** in a reactive way.

**Goals of the project**
- show all needed configuration
- demonstrate 2-way communication `server <-> client` as data streaming
- try to discover bottlenecks in the architecture

## Project overview

Climate Manager is a (demo) application to control air conditioners in distributed locations.

`manager` service receives sensor readings from many `device` services and sends them commands with temperature change.

`manager` is a Spring Boot application with HTTP server, uses MongoDB with RabbitMQ to store data.

`deivce` doesn't start Spring nor HTTP server. It only uses WebFlux in client mode - also for receiving commands from `manager`.

Sensor readings flow: `device(s) -> REST API -> manager -> RabbitMQ -> MongoDB` \
Every device sends 1 reading per second.

Commands flow: `REST API -> manager -> MongoDB -> Change Streams -> manager -> REST API -> device`

`manager` exposes API for receiving commands (should be posted manually) that are firstly saved to MongoDB.
Second endpoint is used by `device` to listen for new commands.

Additionally, every 10s PING command is send to every connected device.

## What it demonstrates

- Producing Flux stream as SSE (Server Sent Events): `StatusController`, `MetricsController`
- Using `ndjson` (Newline Delimited JSON) for exchanging data between `manager/device`
- `ApplicationRunner` reading from RabbitMQ and saving to MongoDB - all reactive: `RabbitToMongoRunner`
- Using `Mono.defer()` to return `404` when entity was not found: `DeviceController`
- Updating data in MongoDB using JPA Query Methods with `@Update`: `DeviceRepository`
- Retries in different places, e.g. waiting for RabbitMQ, for `manager` service
- MongoDB Change Streams with resume feature: `CommandService`
- Detect which clients are connected and which disconnected

## Installation

### MongoDB

This project uses Change Streams which requires replica set configuration.
See [Convert a Standalone to a Replica Set](https://www.mongodb.com/docs/manual/tutorial/convert-standalone-to-replica-set/) for details.

1. Download MongoDB Server and MongoDB Shell for your platform
2. Run server
    ```
    <YOUR_PATH>/bin/mongod \
    --port 27017 \
    --dbpath <DATA_PATH> \
    --replSet rs0 \
    --bind_ip localhost
    ```
3. Initiate replica set using MongoDB Shell
    ```
    <YOUR_PATH>/bin/mongosh \
    --quiet \
    --eval "rs.initiate()"
    ```

### RabbitMQ

It's enough to run in Docker, see docs on [official website](https://www.rabbitmq.com/download.html).

UI is available on http://localhost:15672 with user/pass `guest` / `guest`

## Running

1. Start `manager` service
2. Register device
   ```
   curl -X POST http://localhost:8080/device \
      -H 'Content-Type: application/json' \
      -d "{\"deviceId\":\"unnamed\",\"deviceName\":\"ExampleName\"}"
   ```
3. Start `device` service \
By default `unnamed` device ID is set. It can be configured by `DEVICE_ID` environment variable.
4. Post command to be received by `device` service
   ```
   curl -X POST http://localhost:8080/command/unnamed \
      -H 'Content-Type: application/json' \
      -d "{\"commandType\":\"TEMPERATURE\",\"value\":25}"
   ```

### Running many devices

For observing how it behaves at high load, run it using Bash scripts from `scripts` directory.

`run-many-devices.sh` by default will start 100 `device` instances, registering them before.

### Interfaces

http://localhost:8080/webjars/swagger-ui/index.html

`/status` and `/metrics` endpoints streams statistics in text mode, refreshes every few seconds.

```
curl http://localhost:8080/status

data:+--------------+------------+------------+
data:| Device ID    | Status     | Data count |
data:+--------------+------------+------------+
data:| unnamed      | DOWN       |        703 |
data:| device-101   | RUNNING    |        136 |
data:| device-102   | RUNNING    |        135 |
data:| device-103   | RUNNING    |        134 |
data:| device-104   | RUNNING    |        134 |
data:| device-105   | RUNNING    |        134 |
data:+--------------+------------+------------+
```

```
curl http://localhost:8080/metrics

data:+--------------+-------------+-------------+
data:| Device ID    | TEMPERATURE | HUMIDITY    |
data:+--------------+-------------+-------------+
data:| device-101   |        44.5 |        35.0 |
data:| device-102   |        40.3 |        53.0 |
data:| device-103   |        39.5 |        45.0 |
data:| device-104   |        33.7 |        22.5 |
data:| device-105   |        41.5 |        33.0 |
data:+--------------+-------------+-------------+
```

## Lessons learned

### Bottleneck

Change Streams feature is a bottleneck, because new MongoDB connection is opened for every client.
Increasing connections limit on MongoDB server is problematic and doesn't solve this problem.

Conclusion: Bad architecture for many clients, Change Streams is not intended to use this way.

### Performance

Without Change Streams, I successfully run 200 devices on development machine.
Every device creates separate connection and is sending 1 sensor reading per second.
It means that 200 readings per seconds from 200 simultaneous connections goes through RabbitMQ to be persisted in MongoDB. 
